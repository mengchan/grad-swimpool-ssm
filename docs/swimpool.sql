/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : swimpool

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 04/04/2021 22:05:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for a_admins
-- ----------------------------
DROP TABLE IF EXISTS `a_admins`;
CREATE TABLE `a_admins`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键;自动递增',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `status` tinyint NOT NULL COMMENT '状态:0:禁用,1:启用',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `lastLoginTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上次登陆时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '管理员表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of a_admins
-- ----------------------------
INSERT INTO `a_admins` VALUES (1, '11@11.com', 'sha256$X5F8UaT88fCuy4XojdUFjde4crDJeHFZ$f76e25072220eddb82b8768829f185fdb9b16e1e96aa38f8329f3ea95d79db12', '张', '5456555', 1, '2021-04-02 11:41:41', '2021-04-02 11:41:41', '2021-04-02 11:41:41');

-- ----------------------------
-- Table structure for a_class_desc
-- ----------------------------
DROP TABLE IF EXISTS `a_class_desc`;
CREATE TABLE `a_class_desc`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键;自动递增',
  `goodsId` int NULL DEFAULT NULL COMMENT '产品Id',
  `currBeginDate` datetime NULL DEFAULT NULL COMMENT '上课开始时间',
  `currEndDate` datetime NULL DEFAULT NULL COMMENT '上课结束时间',
  `price` decimal(20, 2) NULL DEFAULT NULL COMMENT '价格',
  `peoNum` int NULL DEFAULT NULL COMMENT '上课人数',
  `status` tinyint NOT NULL COMMENT '状态:0:禁用,1:启用',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `pubTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间(用来排序)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '课程_概要表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of a_class_desc
-- ----------------------------

-- ----------------------------
-- Table structure for a_class_pay
-- ----------------------------
DROP TABLE IF EXISTS `a_class_pay`;
CREATE TABLE `a_class_pay`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键;自动递增',
  `goodsId` int NULL DEFAULT NULL COMMENT '产品Id',
  `classId` int NULL DEFAULT NULL COMMENT '上课id',
  `usersId` int NULL DEFAULT NULL COMMENT '用户Id',
  `beginTime` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `endTime` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `useClassNum` int NULL DEFAULT NULL COMMENT '已经上课数量',
  `totalClassNum` int NULL DEFAULT NULL COMMENT '总课程数量',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `totalPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '总价格',
  `payType` decimal(10, 2) NULL DEFAULT NULL COMMENT '类型:0:节课,1:周课,2:年课',
  `status` tinyint NOT NULL COMMENT '状态:0:未支付,1:已支付;',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `pubTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间(用来排序)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '课程支付表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of a_class_pay
-- ----------------------------

-- ----------------------------
-- Table structure for a_class_peo
-- ----------------------------
DROP TABLE IF EXISTS `a_class_peo`;
CREATE TABLE `a_class_peo`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键;自动递增',
  `classId` int NULL DEFAULT NULL COMMENT '上课id',
  `usersId` int NULL DEFAULT NULL COMMENT '用户Id',
  `status` tinyint NOT NULL COMMENT '状态:0:禁用,1:启用',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `pubTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间(用来排序)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '课程人数表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of a_class_peo
-- ----------------------------

-- ----------------------------
-- Table structure for a_goods
-- ----------------------------
DROP TABLE IF EXISTS `a_goods`;
CREATE TABLE `a_goods`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键;自动递增',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `price` decimal(20, 2) NULL DEFAULT NULL COMMENT '价格',
  `totalPrice` decimal(20, 2) NULL DEFAULT NULL COMMENT '总价格',
  `perLong` int NULL DEFAULT NULL COMMENT '每周课数',
  `weekLong` int NULL DEFAULT NULL COMMENT '时长:12周',
  `peoNum` int NULL DEFAULT NULL COMMENT '人数',
  `souType` tinyint NULL DEFAULT NULL COMMENT '类型:0:课程,1:会员;2:年龄段',
  `status` tinyint NOT NULL COMMENT '状态:0:禁用,1:启用',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `pubTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间(用来排序)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '产品表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of a_goods
-- ----------------------------
INSERT INTO `a_goods` VALUES (1, 'primary school children', 5.00, 300.00, 5, 12, 40, 0, 1, '2021-04-02 09:10:34', '2021-04-02 09:10:34', '2021-04-02 09:10:34');
INSERT INTO `a_goods` VALUES (2, 'beginner', 10.00, 360.00, 3, 12, 30, 0, 1, '2021-04-02 09:15:38', '2021-04-02 09:15:38', '2021-04-02 09:15:38');
INSERT INTO `a_goods` VALUES (3, 'intermediate', 15.00, 360.00, 2, 12, 30, 0, 0, '2021-04-02 09:16:14', '2021-04-02 09:16:14', '2021-04-02 09:16:14');
INSERT INTO `a_goods` VALUES (4, 'advanced', 20.00, 480.00, 2, 12, 30, 0, 1, '2021-04-02 09:16:43', '2021-04-02 09:16:43', '2021-04-02 09:16:43');
INSERT INTO `a_goods` VALUES (5, 'member year', 200.00, 1.00, 1, 1, 0, 1, 1, '2021-04-02 09:22:10', '2021-04-02 09:22:10', '2021-04-02 09:22:10');
INSERT INTO `a_goods` VALUES (6, '1233', 1232.00, 186692352.00, 1232, 123, 123, 1, 1, '2021-04-04 14:48:32', '2021-04-04 17:06:51', '2021-04-04 14:48:32');
INSERT INTO `a_goods` VALUES (7, 'ddd', 11.00, 1331.00, 11, 11, 1, 1, 1, '2021-04-04 15:16:29', '2021-04-04 15:16:29', '2021-04-04 15:16:29');
INSERT INTO `a_goods` VALUES (8, 'aa', 11.00, 1331.00, 11, 11, 11, 0, 1, '2021-04-04 17:05:53', '2021-04-04 17:05:53', '2021-04-04 17:05:53');
INSERT INTO `a_goods` VALUES (9, '11', 10.12, 1224.52, 11, 11, 11, 0, 1, '2021-04-04 17:06:17', '2021-04-04 17:06:17', '2021-04-04 17:06:17');

-- ----------------------------
-- Table structure for a_staff
-- ----------------------------
DROP TABLE IF EXISTS `a_staff`;
CREATE TABLE `a_staff`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键;自动递增',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `sex` smallint NULL DEFAULT NULL COMMENT '性别:0:无,1:男;2:女',
  `status` tinyint NOT NULL COMMENT '状态:0:禁用,1:启用',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `lastLoginTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上次登陆时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '员工表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of a_staff
-- ----------------------------
INSERT INTO `a_staff` VALUES (1, '1', '1', '11', '1', 1, 1, '2021-04-04 17:17:37', '2021-04-04 17:17:37', '2021-04-04 17:17:37');
INSERT INTO `a_staff` VALUES (2, '13@13.com', 'sha256$m6Zc5gSJBmZubMXmp43mEmFFcyDdyvKX$ea0bdceffb3d4b58aa97dde4f5d399eed56f6bc2f2189a5eaabc8f1b1509df53', '123331', '11331112', 1, 1, '2021-04-04 17:24:46', '2021-04-04 17:42:50', '2021-04-04 17:24:46');

-- ----------------------------
-- Table structure for a_users
-- ----------------------------
DROP TABLE IF EXISTS `a_users`;
CREATE TABLE `a_users`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键;自动递增',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `nickName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `sex` smallint NULL DEFAULT NULL COMMENT '性别:0:无,1:男;2:女',
  `loginCount` int NULL DEFAULT NULL COMMENT '登录次数',
  `birthday` date NULL DEFAULT NULL COMMENT '生日:(判断年龄)',
  `school` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学校',
  `status` tinyint NOT NULL COMMENT '状态:0:禁用,1:启用',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `lastLoginTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上次登陆时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of a_users
-- ----------------------------
INSERT INTO `a_users` VALUES (1, '11@11.com', 'sha256$GaUE7gvTgcFDYeymkuZ7LL93wxebuTKh$11c8458944f820ca2ae35c1b536df4343720aa0ea0ceaf4f95605a9b60e1404f', NULL, '111111', NULL, 0, 0, NULL, '111111', 1, '2021-04-04 21:40:48', '2021-04-04 21:40:48', '2021-04-04 21:40:48');

SET FOREIGN_KEY_CHECKS = 1;
