package com.wangsh.head;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wangsh.common.controller.BaseController;
import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.util.ConstatFinalUtil;
import com.wangsh.common.util.PageInfoUtil;
import com.wangsh.users.pojo.AStaff;
import com.wangsh.users.pojo.AUsers;
import com.wangsh.users.service.IUsersDbService;

/**
 * 用户操作的Controller
 * 
 * @author wangshMac
 */
@RestController
@RequestMapping(value = "/users", produces = "text/html;charset=UTF-8")
public class UsersHeadController extends BaseController
{
	@Autowired
	private IUsersDbService usersDbService ;
	
	/*====员工操作开始====*/
	/**
	 * staffList
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/staffList.mvc")
	public String staffList(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==staffList==");
		/* 处理分页 */
		PageInfoUtil pageInfo = this.proccedPageInfo(request);
		/* 处理搜索 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 处理搜索 */
		ApiResponse<AStaff> apiResponse = this.usersDbService.findCondListStaffService(pageInfo, condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * staffInsert
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/staffInsert.mvc")
	public String staffInsert(HttpServletRequest request,HttpServletResponse response,
			AStaff staff) {
		ConstatFinalUtil.SYS_LOGGER.info("==staffInsert==");
		ApiResponse<Object> apiResponse = this.usersDbService.saveOneStaffService(staff);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * staffUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/staffInfo.mvc")
	public String staffInfo(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==staffInfo==");
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AStaff> apiResponse = this.usersDbService.findOneStaffService(condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * staffUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/staffUpdate.mvc")
	public String staffUpdate(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==staffUpdate==");
		
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AStaff> apiResponse = this.usersDbService.findOneStaffService(condMap);
		AStaff staff = apiResponse.getDataOneJava();
		
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String sex = request.getParameter("sex");
		String status = request.getParameter("status");
		
		staff.setEmail(email);
		staff.setName(name);
		staff.setPhone(phone);
		staff.setSex(Byte.valueOf(sex));
		staff.setStatus(Byte.valueOf(status));
		
		ApiResponse<Object> apiDbResponse = this.usersDbService.updateOneStaffService(staff);
		return apiDbResponse.toJSON().toJSONString() ; 
	}
	/*====员工操作结束====*/
	
	/*====用户操作开始====*/
	/**
	 * usersList
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/usersList.mvc")
	public String usersList(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==usersList==");
		/* 处理分页 */
		PageInfoUtil pageInfo = this.proccedPageInfo(request);
		/* 处理搜索 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 处理搜索 */
		ApiResponse<AUsers> apiResponse = this.usersDbService.findCondListUsersService(pageInfo, condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * usersUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/usersInfo.mvc")
	public String usersInfo(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==usersInfo==");
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AUsers> apiResponse = this.usersDbService.findOneUsersService(condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * usersUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/usersUpdate.mvc")
	public String usersUpdate(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==usersUpdate==");
		
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AUsers> apiResponse = this.usersDbService.findOneUsersService(condMap);
		AUsers users = apiResponse.getDataOneJava();
		
		boolean flag = false ;
		String operType = request.getParameter("operType");
		if("status".equalsIgnoreCase(operType)) {
			String status = request.getParameter("status");
			users.setStatus(Byte.valueOf(status));
			flag = true ; 
		}
		ApiResponse<Object> apiDbResponse = new ApiResponse<Object>();
		if(flag)
		{
			apiDbResponse = this.usersDbService.updateOneUsersService(users);
		}
		return apiDbResponse.toJSON().toJSONString() ; 
	}
	/*====用户操作结束====*/
}
