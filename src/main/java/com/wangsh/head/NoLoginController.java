package com.wangsh.head;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wangsh.common.controller.BaseController;
import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.pojo.ApiResponseEnum;
import com.wangsh.common.util.ConstatFinalUtil;
import com.wangsh.users.pojo.AAdmins;
import com.wangsh.users.pojo.AAdminsEnum;
import com.wangsh.users.pojo.AStaff;
import com.wangsh.users.pojo.AUsers;
import com.wangsh.users.service.IUsersDbService;

/**
 * 登陆操作的Controller
 * 
 * @author wangshMac
 */
@RestController
@RequestMapping(produces = "text/html;charset=UTF-8")
public class NoLoginController extends BaseController
{
	@Autowired
	private IUsersDbService usersDbService ;
	
	/**
	 * 登陆操作
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/login.mvc")
	public String login(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		ApiResponse<Object> apiResponse  = new ApiResponse<Object>();
		ConstatFinalUtil.SYS_LOGGER.info("==login==");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String usersType = request.getParameter("usersType");
		if(usersType == null || "".equalsIgnoreCase(usersType))
		{
			usersType = "0" ; 
		}
		
		Map<String,Object> condMap = new HashMap<String,Object>();
		condMap.put("email", email);
		if(ApiResponseEnum.USERTYPE_STAFF.getStatus() == Byte.valueOf(usersType)) {
			/* 员工登陆 */
			ApiResponse<AStaff> staffResponse = this.usersDbService.findOneStaffService(condMap);
			AStaff staff = staffResponse.getDataOneJava();
			if(staff != null) {
				if(this.encryptUtil.checkStr(password, staff.getPassword())) {
					if(staff.getStatus() == AAdminsEnum.STATUS_ENABLE.getStatus()) {
						/* 跳转到成功页面 */
						return staffResponse.toJSON().toJSONString() ; 
					}else {
						apiResponse.setCode(ApiResponseEnum.INFO_ACCOUNT_DISABLED.getStatus());
					}
				}else {
					apiResponse.setCode(ApiResponseEnum.INFO_PASS_INCORRECT.getStatus());
				}
			}else {
				apiResponse.setCode(ApiResponseEnum.INFO_EMAIL_NOT_EXISTS.getStatus());
			}
		}else if(ApiResponseEnum.USERTYPE_USER.getStatus() == Byte.valueOf(usersType)) {
			/* 用户登陆 */
			ApiResponse<AUsers> usersApiResponse = this.usersDbService.findOneUsersService(condMap);
			AUsers users = usersApiResponse.getDataOneJava();
			if(users != null) {
				if(this.encryptUtil.checkStr(password, users.getPassword())) {
					if(users.getStatus() == AAdminsEnum.STATUS_ENABLE.getStatus()) {
						/* 跳转到成功页面 */
						return usersApiResponse.toJSON().toJSONString() ; 
					}else {
						apiResponse.setCode(ApiResponseEnum.INFO_ACCOUNT_DISABLED.getStatus());
					}
				}else {
					apiResponse.setCode(ApiResponseEnum.INFO_PASS_INCORRECT.getStatus());
				}
			}else {
				apiResponse.setCode(ApiResponseEnum.INFO_EMAIL_NOT_EXISTS.getStatus());
			}
		}else if(ApiResponseEnum.USERTYPE_ADMINS.getStatus() == Byte.valueOf(usersType)) {
			/* 管理员登陆 */
			ApiResponse<AAdmins> adminsResponse = this.usersDbService.findOneAdminsService(condMap);
			AAdmins admins = adminsResponse.getDataOneJava();
			if(admins != null) {
				if(this.encryptUtil.checkStr(password, admins.getPassword())) {
					if(admins.getStatus() == AAdminsEnum.STATUS_ENABLE.getStatus()) {
						/* 跳转到成功页面 */
						return adminsResponse.toJSON().toJSONString() ; 
					}else {
						apiResponse.setCode(ApiResponseEnum.INFO_ACCOUNT_DISABLED.getStatus());
					}
				}else {
					apiResponse.setCode(ApiResponseEnum.INFO_PASS_INCORRECT.getStatus());
				}
			}else {
				apiResponse.setCode(ApiResponseEnum.INFO_EMAIL_NOT_EXISTS.getStatus());
			}
		}else
		{
			/* 无效操作 */
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		
		/* 设置提示信息 */
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), 
				Collections.EMPTY_MAP);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * usersInsert
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/usersInsert.mvc")
	public String usersInsert(HttpServletRequest request,HttpServletResponse response,
			AUsers users) {
		ConstatFinalUtil.SYS_LOGGER.info("==usersInsert==");
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		if(users.getEmail() == null || "".equalsIgnoreCase(users.getEmail())) {
			apiResponse.setCode(ApiResponseEnum.INFO_EMAIL_NO_EMPTY.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse.toJSON().toJSONString() ; 
		}
		
		String birthdayStr = request.getParameter("birthdayStr");
		users.setBirthday(this.dateFormatUtil.strDate(birthdayStr, ConstatFinalUtil.DATE_FORMAT));
		
		users.setPassword(this.encryptUtil.encodeStr(users.getPassword()));
		apiResponse = this.usersDbService.saveOneUsersService(users);
		return apiResponse.toJSON().toJSONString() ; 
	}
}
