package com.wangsh.head;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wangsh.clazz.pojo.AClassDesc;
import com.wangsh.clazz.pojo.AClassPay;
import com.wangsh.clazz.pojo.AClassPeo;
import com.wangsh.clazz.pojo.AGoods;
import com.wangsh.clazz.service.IClassDbService;
import com.wangsh.clazz.service.IClassOperService;
import com.wangsh.common.controller.BaseController;
import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.util.ConstatFinalUtil;
import com.wangsh.common.util.PageInfoUtil;

/**
 * 课程操作的Controller
 * 
 * @author wangshMac
 */
@RestController
@RequestMapping(value = "/class", produces = "text/html;charset=UTF-8")
public class ClassHeadController extends BaseController
{
	@Autowired
	private IClassDbService classDbService ;
	@Autowired
	private IClassOperService classOperService;
	
	/*====产品操作开始====*/
	/**
	 * goodsList
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/goodsList.mvc")
	public String goodsList(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==goodsList==");
		/* 处理分页 */
		PageInfoUtil pageInfo = this.proccedPageInfo(request);
		/* 处理搜索 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 处理搜索 */
		ApiResponse<AGoods> apiResponse = new ApiResponse<AGoods>();
		
		String pageFlag = request.getParameter("pageFlag");
		if("false".equalsIgnoreCase(pageFlag))
		{
			apiResponse = this.classDbService.findCondListGoodsService(null, condMap);
		}else
		{
			apiResponse = this.classDbService.findCondListGoodsService(pageInfo, condMap);
		}
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * goodsInsert
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/goodsInsert.mvc")
	public String goodsInsert(HttpServletRequest request,HttpServletResponse response,
			AGoods goods) {
		ConstatFinalUtil.SYS_LOGGER.info("==goodsInsert==");
		ApiResponse<Object> apiResponse = this.classDbService.saveOneGoodsService(goods);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * goodsUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/goodsInfo.mvc")
	public String goodsInfo(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==goodsInfo==");
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AGoods> apiResponse = this.classDbService.findOneGoodsService(condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * goodsUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/goodsUpdate.mvc")
	public String goodsUpdate(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==goodsUpdate==");
		
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AGoods> apiResponse = this.classDbService.findOneGoodsService(condMap);
		AGoods goods = apiResponse.getDataOneJava();
		
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String perLong = request.getParameter("perLong");
		String weekLong = request.getParameter("weekLong");
		String peoNum = request.getParameter("peoNum");
		String souType = request.getParameter("souType");
		String status = request.getParameter("status");
		
		goods.setName(name);
		goods.setPrice(Double.valueOf(price));
		goods.setPerLong(Integer.valueOf(perLong));
		goods.setWeekLong(Integer.valueOf(weekLong));
		goods.setPeoNum(Integer.valueOf(peoNum));
		goods.setSouType(Byte.valueOf(souType));
		goods.setStatus(Byte.valueOf(status));
		
		ApiResponse<Object> apiDbResponse = this.classDbService.updateOneGoodsService(goods);
		return apiDbResponse.toJSON().toJSONString() ; 
	}
	/*====产品操作结束====*/
	
	/*====课程支付开始====*/
	/**
	 * payList
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/payList.mvc")
	public String payList(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==payList==");
		/* 处理分页 */
		PageInfoUtil pageInfo = this.proccedPageInfo(request);
		/* 处理搜索 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 处理搜索 */
		ApiResponse<AClassPay> apiResponse = this.classDbService.findCondListClassPayService(pageInfo, condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * computePrice
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/payCompute.mvc")
	public String payCompute(HttpServletRequest request,HttpServletResponse response,
			AClassPay classPay) {
		ConstatFinalUtil.SYS_LOGGER.info("==payCompute==");
		
		String beginTimeStr = request.getParameter("beginTimeStr");
		classPay.setBeginTime(this.dateFormatUtil.strDateTime(beginTimeStr));
		
		classPay = this.classOperService.computePrice(classPay);
		return classPay.toJSON().toJSONString() ; 
	}
	
	
	/**
	 * payInsert
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/payInsert.mvc")
	public String payInsert(HttpServletRequest request,HttpServletResponse response,
			AClassPay classPay) {
		ConstatFinalUtil.SYS_LOGGER.info("==payInsert==");
		
		String beginTimeStr = request.getParameter("beginTimeStr");
		classPay.setBeginTime(this.dateFormatUtil.strDateTime(beginTimeStr));
		
		String endTimeStr = request.getParameter("endTimeStr");
		classPay.setEndTime(this.dateFormatUtil.strDateTime(endTimeStr));
		
		ApiResponse<Object> apiResponse = this.classDbService.saveOneClassPayService(classPay);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * payInfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/payInfo.mvc")
	public String payInfo(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==payInfo==");
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AClassPay> apiResponse = this.classDbService.findOneClassPayService(condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * payUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/payUpdate.mvc")
	public String payUpdate(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==goodsUpdate==");
		
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AClassPay> apiResponse = this.classDbService.findOneClassPayService(condMap);
		AClassPay Classpays = apiResponse.getDataOneJava();
		
		String goodsId = request.getParameter("goodsId");
		String classId = request.getParameter("classId");
		String usersId = request.getParameter("usersId");
		String useClassNum = request.getParameter("useClassNum");
		String totalClassNum = request.getParameter("totalClassNum");
		String price = request.getParameter("price");
		String totalPrice = request.getParameter("totalPrice");
		String status = request.getParameter("status");
		
		
		Classpays.setGoodsId(Integer.valueOf(goodsId));		
		Classpays.setClassId(Integer.valueOf(classId));
		Classpays.setUsersId(Integer.valueOf(usersId));
		Classpays.setUseClassNum(Integer.valueOf(useClassNum));
		Classpays.setTotalClassNum(Integer.valueOf(totalClassNum));
		Classpays.setPrice(Double.valueOf(price));
		Classpays.setTotalPrice(Integer.valueOf(totalPrice));
		Classpays.setStatus(Byte.valueOf(status));
		
		ApiResponse<Object> apiDbResponse = this.classDbService.updateOneClassPayService(Classpays);
		return apiDbResponse.toJSON().toJSONString() ; 
	}
	/*====课程支付操作结束====*/
	
	/*====课程概要操作开始====*/
	/**
	 * classDescList
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classDescList.mvc")
	public String classDescList(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==classDescList==");
		/* 处理分页 */
		PageInfoUtil pageInfo = this.proccedPageInfo(request);
		/* 处理搜索 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 处理搜索 */
		ApiResponse<AClassDesc> apiResponse = this.classDbService.findCondListClassDescService(pageInfo, condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * classDescInsert
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classDescInsert.mvc")
	public String classDescInsert(HttpServletRequest request,HttpServletResponse response,
			AClassDesc classDesc) {
		ConstatFinalUtil.SYS_LOGGER.info("==classDescInsert==");
		
		String currBeginDateStr = request.getParameter("currBeginDateStr");
		String currEndDateStr = request.getParameter("currEndDateStr");
		
		classDesc.setCurrBeginDate(this.dateFormatUtil.strDateTime(currBeginDateStr));
		classDesc.setCurrEndDate(this.dateFormatUtil.strDateTime(currEndDateStr));
		
		ApiResponse<Object> apiResponse = this.classDbService.saveOneClassDescService(classDesc);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * classDescUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classDescInfo.mvc")
	public String classDescInfo(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==classDescInfo==");
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AClassDesc> apiResponse = this.classDbService.findOneClassDescService(condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * classDescUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classDescUpdate.mvc")
	public String classDescUpdate(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==classDescUpdate==");
		
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AClassDesc> apiResponse = this.classDbService.findOneClassDescService(condMap);
		AClassDesc classDesc = apiResponse.getDataOneJava();
		
		String goodsId = request.getParameter("goodsId");
		String currBeginDateStr = request.getParameter("currBeginDateStr");
		String currEndDateStr = request.getParameter("currEndDateStr");
		String status = request.getParameter("status");
		
		classDesc.setGoodsId(Integer.valueOf(goodsId));
		classDesc.setCurrBeginDate(this.dateFormatUtil.strDateTime(currBeginDateStr));
		classDesc.setCurrEndDate(this.dateFormatUtil.strDateTime(currEndDateStr));
		classDesc.setStatus(Byte.valueOf(status));
		
		ApiResponse<Object> apiDbResponse = this.classDbService.updateOneClassDescService(classDesc);
		return apiDbResponse.toJSON().toJSONString() ; 
	}
	
	
	/*====课程概要操作结束====*/
	
	
	
	
	/*====课程人数操作开始====*/
	/**
	 * classPeoList
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classPeoList.mvc")
	public String classPeoList(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==classPeoList==");
		/* 处理分页 */
		PageInfoUtil pageInfo = this.proccedPageInfo(request);
		/* 处理搜索 */
		Map<String, Object> condMap = this.proccedSearch(request);
		/* 处理搜索 */
		ApiResponse<AClassPeo> apiResponse = this.classDbService.findCondListClassPeoService(pageInfo, condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * classPeoInsert
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classPeoInsert.mvc")
	public String classPeoInsert(HttpServletRequest request,HttpServletResponse response,
			AClassPeo classPeo) {
		ConstatFinalUtil.SYS_LOGGER.info("==classPeoInsert==");
		
		String calssId = request.getParameter("classId");
		String usersId = request.getParameter("usersId");
		
		classPeo.setClassId(Integer.valueOf(calssId));
		classPeo.setUsersId(Integer.valueOf(usersId));
		
		ApiResponse<Object> apiResponse = this.classDbService.saveOneClassPeoService(classPeo);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * classPeoInfo
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classPeoInfo.mvc")
	public String classPeoInfo(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==classPeoInfo==");
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AClassPeo> apiResponse = this.classDbService.findOneClassPeoService(condMap);
		return apiResponse.toJSON().toJSONString() ; 
	}
	
	/**
	 * classPeoUpdate
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/classPeoUpdate.mvc")
	public String classPeoUpdate(HttpServletRequest request,HttpServletResponse response) {
		ConstatFinalUtil.SYS_LOGGER.info("==classPeoUpdate==");
		
		String id = request.getParameter("id");
		
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("id", id);
		
		ApiResponse<AClassPeo> apiResponse = this.classDbService.findOneClassPeoService(condMap);
		AClassPeo classPeo = apiResponse.getDataOneJava();
		
		String calssId = request.getParameter("classId");
		String usersId = request.getParameter("usersId");
		String status = request.getParameter("status");
		
		classPeo.setClassId(Integer.valueOf(calssId));
		classPeo.setUsersId(Integer.valueOf(usersId));
		classPeo.setStatus(Byte.valueOf(status));
		
		ApiResponse<Object> apiDbResponse = this.classDbService.updateOneClassPeoService(classPeo);
		return apiDbResponse.toJSON().toJSONString() ; 
	}
	
	/*====课程人数操作结束====*/
}
