package com.wangsh.clazz.dao;

import com.wangsh.clazz.pojo.AClassPeo;
import com.wangsh.common.dao.IBaseDao;

/**
 * 课程学生 dao
 * 
 * @author Tm
 */
public interface IAClassPeoDao extends IBaseDao<AClassPeo> {
	
}
