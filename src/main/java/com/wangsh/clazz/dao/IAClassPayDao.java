package com.wangsh.clazz.dao;

import com.wangsh.clazz.pojo.AClassPay;
import com.wangsh.common.dao.IBaseDao;

/**
 * 课程支付 dao
 * 
 * @author Tm
 */
public interface IAClassPayDao extends IBaseDao<AClassPay> {
	
}
