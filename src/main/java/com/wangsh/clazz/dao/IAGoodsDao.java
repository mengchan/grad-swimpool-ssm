package com.wangsh.clazz.dao;

import com.wangsh.clazz.pojo.AGoods;
import com.wangsh.common.dao.IBaseDao;

/**
 * 产品 dao
 * 
 * @author Tm
 */
public interface IAGoodsDao extends IBaseDao<AGoods> {
	
}
