package com.wangsh.clazz.dao;

import com.wangsh.clazz.pojo.AClassDesc;
import com.wangsh.common.dao.IBaseDao;

/**
 * 课程概要 dao
 * 
 * @author Tm
 */
public interface IAClassDescDao extends IBaseDao<AClassDesc> {
	
}
