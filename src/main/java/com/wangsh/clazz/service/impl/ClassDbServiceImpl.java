package com.wangsh.clazz.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wangsh.clazz.dao.IAClassDescDao;
import com.wangsh.clazz.dao.IAClassPayDao;
import com.wangsh.clazz.dao.IAClassPeoDao;
import com.wangsh.clazz.dao.IAGoodsDao;
import com.wangsh.clazz.pojo.AClassDesc;
import com.wangsh.clazz.pojo.AClassPay;
import com.wangsh.clazz.pojo.AClassPeo;
import com.wangsh.clazz.pojo.AClassPeoEnum;
import com.wangsh.clazz.pojo.AGoods;
import com.wangsh.clazz.service.IClassDbService;
import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.pojo.ApiResponseEnum;
import com.wangsh.common.pojo.JSONEnum;
import com.wangsh.common.service.BaseServiceImpl;
import com.wangsh.common.util.ConstatFinalUtil;
import com.wangsh.common.util.PageInfoUtil;

/**
 * 课程 Db Service Impl
 * @author Tm
 *
 */
@Service("classDbService")
public class ClassDbServiceImpl extends BaseServiceImpl implements IClassDbService {
	@Resource
	private IAGoodsDao goodsDao;
	@Resource
	private IAClassDescDao classDescDao;
	@Resource
	private IAClassPayDao classPayDao;
	@Resource
	private IAClassPeoDao classPeoDao;

	@Override
	public ApiResponse<Object> saveOneGoodsService(AGoods goods) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		goods.setTotalPrice(goods.getPrice() * goods.getWeekLong() * goods.getPerLong() );
		
		goods.setCreateTime(new Date());
		goods.setUpdateTime(new Date());
		if(goods.getPubTime() == null) {
			goods.setPubTime(new Date());
		}

		int res = this.goodsDao.save(goods);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), goods.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneGoodsService(AGoods goods) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		goods.setTotalPrice(goods.getPrice() * goods.getWeekLong() * goods.getPerLong() );
		
		if(goods.getPubTime() == null) {
			goods.setPubTime(new Date());
		}

		goods.setUpdateTime(new Date());
		int res = this.goodsDao.update(goods);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), goods.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneGoodsService(Map<String, Object> condMap) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.goodsDao.delete(condMap);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AGoods> findOneGoodsService(Map<String, Object> condMap) {
		ApiResponse<AGoods> apiResponse = new ApiResponse<AGoods>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		Map<String, AGoods> data = new HashMap<String, AGoods>();
		AGoods goods = this.goodsDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), goods);
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(goods);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AGoods> findCondListGoodsService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap) {
		ApiResponse<AGoods> apiResponse = new ApiResponse<AGoods>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AGoods> goodsList = Collections.EMPTY_LIST;

		Map<String, List<AGoods>> dataList = new HashMap<String, List<AGoods>>();
		/* 对关键字进行模糊匹配 */
		if (condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + "")) {
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}

		/* 设置分页相关信息 */
		if (pageInfoUtil != null) {
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			goodsList = this.goodsDao.findList(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		} else {
			goodsList = this.goodsDao.findList(condMap);
		}

		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), goodsList);
		apiResponse.setDataListJava(goodsList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> saveOneClassDescService(AClassDesc classDesc) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();

		classDesc.setCreateTime(new Date());
		classDesc.setUpdateTime(new Date());
		if(classDesc.getPubTime() == null) {
			classDesc.setPubTime(new Date());
		}
		
		/* 根据goodsId查询goods */
		Map<String,Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", classDesc.getGoodsId());
		ApiResponse<AGoods> goodsApiResponse = this.findOneGoodsService(condMap);
		AGoods goods = goodsApiResponse.getDataOneJava();
		if(goods != null) {
			classDesc.setPrice(goods.getPrice());
			classDesc.setPeoNum(goods.getPeoNum());
		}
		
		int res = this.classDescDao.save(classDesc);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), classDesc.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneClassDescService(AClassDesc classDesc) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();

		if(classDesc.getPubTime() == null) {
			classDesc.setPubTime(new Date());
		}
		
		/* 根据goodsId查询goods */
		Map<String,Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", classDesc.getGoodsId());
		ApiResponse<AGoods> goodsApiResponse = this.findOneGoodsService(condMap);
		AGoods goods = goodsApiResponse.getDataOneJava();
		if(goods != null) {
			classDesc.setPrice(goods.getPrice());
			classDesc.setPeoNum(goods.getPeoNum());
		}
		
		classDesc.setUpdateTime(new Date());
		int res = this.classDescDao.update(classDesc);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), classDesc.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneClassDescService(Map<String, Object> condMap) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.classDescDao.delete(condMap);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClassDesc> findOneClassDescService(Map<String, Object> condMap) {
		ApiResponse<AClassDesc> apiResponse = new ApiResponse<AClassDesc>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		Map<String, AClassDesc> data = new HashMap<String, AClassDesc>();
		AClassDesc classDesc = this.classDescDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), classDesc);
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(classDesc);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClassDesc> findCondListClassDescService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap) {
		ApiResponse<AClassDesc> apiResponse = new ApiResponse<AClassDesc>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AClassDesc> classDescList = Collections.EMPTY_LIST;

		Map<String, List<AClassDesc>> dataList = new HashMap<String, List<AClassDesc>>();
		/* 对关键字进行模糊匹配 */
		if (condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + "")) {
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}

		/* 设置分页相关信息 */
		if (pageInfoUtil != null) {
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			classDescList = this.classDescDao.findList(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		} else {
			classDescList = this.classDescDao.findList(condMap);
		}

		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), classDescList);
		apiResponse.setDataListJava(classDescList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> saveOneClassPayService(AClassPay classPay) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();

		classPay.setCreateTime(new Date());
		classPay.setUpdateTime(new Date());
		if(classPay.getPubTime() == null) {
			classPay.setPubTime(new Date());
		}
		
		/* usersId,begtimeCond > begtime , begtimeCond < endTime */
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.put("usersId", classPay.getUsersId());
		condMap.put("beginTimeCond", classPay.getBeginTime());
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		this.findCondListClassPayService(pageInfoUtil, condMap);
		if(pageInfoUtil.getTotalRecord() > 0 )
		{
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse ; 
		}

		int res = this.classPayDao.save(classPay);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), classPay.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneClassPayService(AClassPay classPay) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(classPay.getPubTime() == null) {
			classPay.setPubTime(new Date());
		}

		classPay.setUpdateTime(new Date());
		int res = this.classPayDao.update(classPay);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), classPay.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneClassPayService(Map<String, Object> condMap) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.classPayDao.delete(condMap);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClassPay> findOneClassPayService(Map<String, Object> condMap) {
		ApiResponse<AClassPay> apiResponse = new ApiResponse<AClassPay>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		Map<String, AClassPay> data = new HashMap<String, AClassPay>();
		AClassPay classPay = this.classPayDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), classPay);
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(classPay);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClassPay> findCondListClassPayService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap) {
		ApiResponse<AClassPay> apiResponse = new ApiResponse<AClassPay>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AClassPay> classPayList = Collections.EMPTY_LIST;

		Map<String, List<AClassPay>> dataList = new HashMap<String, List<AClassPay>>();
		/* 对关键字进行模糊匹配 */
		if (condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + "")) {
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}

		/* 设置分页相关信息 */
		if (pageInfoUtil != null) {
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			classPayList = this.classPayDao.findList(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		} else {
			classPayList = this.classPayDao.findList(condMap);
		}

		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), classPayList);
		apiResponse.setDataListJava(classPayList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
	
	@Override
	public ApiResponse<Object> saveOneClassPeoService(AClassPeo classPeo) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		/* classid,usersId,status */
		Map<String,Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("usersId", classPeo.getUsersId());
		condMap.put("status", AClassPeoEnum.STATUS_ENABLE.getStatus());
		condMap.put("classId", classPeo.getClassId());
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		this.findCondListClassPeoService(pageInfoUtil, condMap);
		if(pageInfoUtil.getTotalRecord() > 0 ) {
			apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
			apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
			return apiResponse;
		}

		classPeo.setCreateTime(new Date());
		classPeo.setUpdateTime(new Date());
		if(classPeo.getPubTime() == null) {
			classPeo.setPubTime(new Date());
		}
		
		classPeo.setStatus(AClassPeoEnum.STATUS_ENABLE.getStatus());

		int res = this.classPeoDao.save(classPeo);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), classPeo.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> updateOneClassPeoService(AClassPeo classPeo) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		
		if(classPeo.getStatus() == AClassPeoEnum.STATUS_ENABLE.getStatus()) 
		{
			/* classid,usersId,status */
			Map<String,Object> condMap = new HashMap<String, Object>();
			condMap.clear();
			condMap.put("usersId", classPeo.getUsersId());
			condMap.put("status", AClassPeoEnum.STATUS_ENABLE.getStatus());
			condMap.put("classId", classPeo.getClassId());
			PageInfoUtil pageInfoUtil = new PageInfoUtil();
			this.findCondListClassPeoService(pageInfoUtil, condMap);
			if(pageInfoUtil.getTotalRecord() > 0 ) {
				apiResponse.setCode(ApiResponseEnum.INFO_RECORD_EXISTS.getStatus());
				apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
				return apiResponse;
			}
		}
		
		if(classPeo.getPubTime() == null) {
			classPeo.setPubTime(new Date());
		}

		classPeo.setUpdateTime(new Date());
		int res = this.classPeoDao.update(classPeo);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			dataMap.put(JSONEnum.ID.getName(), classPeo.getId());
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<Object> deleteOneClassPeoService(Map<String, Object> condMap) {
		ApiResponse<Object> apiResponse = new ApiResponse<Object>();
		int res = this.classPeoDao.delete(condMap);
		if (res > 0) {
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put(JSONEnum.EFFECT.getName(), res);
			apiResponse.setDataOne(dataMap);

			apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		} else {
			apiResponse.setCode(ApiResponseEnum.STATUS_FAILED.getStatus());
		}
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClassPeo> findOneClassPeoService(Map<String, Object> condMap) {
		ApiResponse<AClassPeo> apiResponse = new ApiResponse<AClassPeo>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		Map<String, AClassPeo> data = new HashMap<String, AClassPeo>();
		AClassPeo classPeo = this.classPeoDao.findOne(condMap);
		data.put(ApiResponseEnum.NAME_ONE.getName(), classPeo);
		/* 将结果放到JSON中 */
		apiResponse.setDataOne(data);
		apiResponse.setDataOneJava(classPeo);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}

	@Override
	public ApiResponse<AClassPeo> findCondListClassPeoService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap) {
		ApiResponse<AClassPeo> apiResponse = new ApiResponse<AClassPeo>();
		apiResponse.setCode(ApiResponseEnum.STATUS_SUCCESS.getStatus());
		List<AClassPeo> classPeoList = Collections.EMPTY_LIST;

		Map<String, List<AClassPeo>> dataList = new HashMap<String, List<AClassPeo>>();
		/* 对关键字进行模糊匹配 */
		if (condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + "")) {
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}

		/* 设置分页相关信息 */
		if (pageInfoUtil != null) {
			/* 设置分页信息 */
			Page page = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			classPeoList = this.classPeoDao.findList(condMap);
			pageInfoUtil.setTotalRecord(Integer.valueOf(page.getTotal() + ""));
			/* 向接口中放分页信息 */
			apiResponse.setPageInfoUtil(pageInfoUtil);
		} else {
			classPeoList = this.classPeoDao.findList(condMap);
		}

		/* 将结果放到JSON中 */
		dataList.put(ApiResponseEnum.NAME_LIST.getName(), classPeoList);
		apiResponse.setDataListJava(classPeoList);
		apiResponse.setDataList(dataList);
		apiResponse.setInfo(ConstatFinalUtil.INFO_JSON.getString(apiResponse.getCode() + ""), Collections.EMPTY_MAP);
		return apiResponse;
	}
}
