package com.wangsh.clazz.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.wangsh.clazz.pojo.AClassPay;
import com.wangsh.clazz.pojo.AClassPayEnum;
import com.wangsh.clazz.pojo.AGoods;
import com.wangsh.clazz.service.IClassDbService;
import com.wangsh.clazz.service.IClassOperService;
import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.service.BaseServiceImpl;
import com.wangsh.users.pojo.AUsers;
import com.wangsh.users.pojo.AUsersEnum;
import com.wangsh.users.service.IUsersDbService;

/**
 * 课程 Oper Service Impl
 * @author Tm
 *
 */
@Service("classOperService")
public class ClassOperServiceImpl<T> extends BaseServiceImpl implements IClassOperService {

	@Resource
	private IClassDbService classDbService;
	@Resource
	private IUsersDbService usersDbService;
	
	@Override
	public AClassPay computePrice(AClassPay cond)
	{
		int goodsId = cond.getGoodsId();
		int classId = cond.getClassId() ;
		int usersId = cond.getUsersId() ; 
		byte payType = cond.getPayType() ; 
		
		
		Map<String,Object> condMap = new HashMap<String,Object>();
		// Goods
		condMap.clear();
		condMap.put("id", goodsId);
		ApiResponse<AGoods> goodsApiResponse = this.classDbService.findOneGoodsService(condMap);
		AGoods goods = goodsApiResponse.getDataOneJava() ; 
		double totalPrice = goods.getTotalPrice() ; 
		
		//users
		condMap.clear();
		condMap.put("id", usersId);
		ApiResponse<AUsers> usersApiResponse = this.usersDbService.findOneUsersService(condMap);
		AUsers users = usersApiResponse.getDataOneJava() ;
		
		Date currDate = cond.getBeginTime() ; 
		Calendar calendar = Calendar.getInstance() ; 
		calendar.setTime(currDate);
		
		double discount = 0 ;  
		/**
		 * 游泳池的会员费为每年200欧元。如果客户是成为游泳池会员，可以享受50元的优惠用户上课打折10%
			课程可以每周付费，也可以在课程开始前购买并付费（所有课程都是12周，在课程开始前购买，客户可以获得10%的折扣）。
			允许所有人使用游泳池（散客），价格取决于年龄组（儿童和60岁以上者5欧元，其他人10欧元）。
		 */
		if(AClassPayEnum.PAYTYPE_WEEK.getStatus() == payType) {
			/* week, */
			discount = discount + totalPrice * 0.1  ; 
			
			calendar.add(Calendar.WEEK_OF_MONTH, 1);
			cond.setEndTime(calendar.getTime());
		}else if(AClassPayEnum.PAYTYPE_YEAR.getStatus() == payType) {
			/* week, */
			calendar.add(Calendar.YEAR, 1);
			cond.setEndTime(calendar.getTime());
		}else
		{
			/* day */
			cond.setEndTime(calendar.getTime());
		}
		
		if(users != null) {
			/* member */
			if(users.getMemFlag() == AUsersEnum.MEMFLAG_YES.getStatus() && AClassPayEnum.PAYTYPE_YEAR.getStatus() == payType) {
				discount = discount + 50 ;  
			}
			
			/* age */
			if(users.getAge() > 60 )
			{
				discount = discount + 5 ; 
			}
		}
		
		/* result */
		AClassPay result = new AClassPay();
		result.setDiscount(discount);
		result.setPrice(goods.getPrice());
		/* 总共课程数 */
		result.setTotalClassNum(goods.getPerLong() * goods.getWeekLong() );
		result.setTotalPrice(totalPrice);
		result.setDealPrice(result.getTotalPrice() - result.getDiscount() );
		result.setBeginTime(cond.getBeginTime());
		result.setEndTime(cond.getEndTime());
		
		return result;
	}
}
