package com.wangsh.clazz.service;

import com.wangsh.clazz.pojo.AClassPay;

/**
 * 课程 Oper Service
 * @author Tm
 *
 */
public interface IClassOperService {
	/**
	 * 根据classId和goodsId来计算价格
	 * 要考虑 支付类型
	 * 
	 * @return
	 */
	AClassPay computePrice(AClassPay cond);
}
