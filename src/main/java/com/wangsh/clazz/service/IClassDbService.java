package com.wangsh.clazz.service;

import java.util.Map;

import com.wangsh.clazz.pojo.AClassDesc;
import com.wangsh.clazz.pojo.AClassPay;
import com.wangsh.clazz.pojo.AClassPeo;
import com.wangsh.clazz.pojo.AGoods;
import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.util.PageInfoUtil;

/**
 * 课程 Db Service
 * @author Tm
 *
 */
public interface IClassDbService {
	/* == 产品操作开始 == */
	/**
	 * 保存一条产品
	 * 
	 * @param goods
	 * @return
	 */
	ApiResponse<Object> saveOneGoodsService(AGoods goods);

	/**
	 * 更新一条产品
	 * 
	 * @param goods
	 * @return
	 */
	ApiResponse<Object> updateOneGoodsService(AGoods goods);

	/**
	 * 删除一条产品
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> deleteOneGoodsService(Map<String, Object> condMap);

	/**
	 * 查询一条产品
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AGoods> findOneGoodsService(Map<String, Object> condMap);

	/**
	 * 查询多条产品
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AGoods> findCondListGoodsService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/* == 产品操作结束 == */
	
	/* == 课程概要操作开始 == */
	/**
	 * 保存一条课程概要
	 * 
	 * @param classDesc
	 * @return
	 */
	ApiResponse<Object> saveOneClassDescService(AClassDesc classDesc);

	/**
	 * 更新一条课程概要
	 * 
	 * @param classDesc
	 * @return
	 */
	ApiResponse<Object> updateOneClassDescService(AClassDesc classDesc);

	/**
	 * 删除一条课程概要
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> deleteOneClassDescService(Map<String, Object> condMap);

	/**
	 * 查询一条课程概要
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassDesc> findOneClassDescService(Map<String, Object> condMap);

	/**
	 * 查询多条课程概要
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassDesc> findCondListClassDescService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/* == 课程概要操作结束 == */
	
	/* == 课程支付操作开始 == */
	/**
	 * 保存一条课程支付
	 * 
	 * @param classPay
	 * @return
	 */
	ApiResponse<Object> saveOneClassPayService(AClassPay classPay);

	/**
	 * 更新一条课程支付
	 * 
	 * @param classPay
	 * @return
	 */
	ApiResponse<Object> updateOneClassPayService(AClassPay classPay);

	/**
	 * 删除一条课程支付
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> deleteOneClassPayService(Map<String, Object> condMap);

	/**
	 * 查询一条课程支付
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassPay> findOneClassPayService(Map<String, Object> condMap);

	/**
	 * 查询多条课程支付
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassPay> findCondListClassPayService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/* == 课程支付操作结束 == */
	
	/* == 课程学生操作开始 == */
	/**
	 * 保存一条课程学生
	 * 
	 * @param classPeo
	 * @return
	 */
	ApiResponse<Object> saveOneClassPeoService(AClassPeo classPeo);

	/**
	 * 更新一条课程学生
	 * 
	 * @param classPeo
	 * @return
	 */
	ApiResponse<Object> updateOneClassPeoService(AClassPeo classPeo);

	/**
	 * 删除一条课程学生
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> deleteOneClassPeoService(Map<String, Object> condMap);

	/**
	 * 查询一条课程学生
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassPeo> findOneClassPeoService(Map<String, Object> condMap);

	/**
	 * 查询多条课程学生
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AClassPeo> findCondListClassPeoService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/* == 课程学生操作结束 == */
}
