package com.wangsh.clazz.pojo;

/**
 * 课程支付 枚举
 * @author Tm
 *
 */
public enum AClassPayEnum {

	STATUS_UNPAY("unpay", Byte.valueOf("0")),
	STATUS_PAYED("payed", Byte.valueOf("1")),
	STATUS_CANCEL("cancel", Byte.valueOf("2")), 
	
	/* 类型:0:节课,1:周课,2:年课 */
	PAYTYPE_PER("class", Byte.valueOf("0")), 
	PAYTYPE_WEEK("week", Byte.valueOf("1")), 
	PAYTYPE_YEAR("year", Byte.valueOf("2")), 
	
	;

	private String name;
	private byte status;

	private AClassPayEnum(String name, byte status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
}
