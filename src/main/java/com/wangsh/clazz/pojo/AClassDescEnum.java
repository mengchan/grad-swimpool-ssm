package com.wangsh.clazz.pojo;

/**
 * 课程概要 枚举
 * @author Tm
 *
 */
public enum AClassDescEnum {

	STATUS_DISABLED("disable", Byte.valueOf("0")),
	STATUS_ENABLE("enable", Byte.valueOf("1")), 
	
	;

	private String name;
	private byte status;

	private AClassDescEnum(String name, byte status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
}
