package com.wangsh.clazz.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.common.pojo.BasePojo;

/**
 * 产品 Pojo
 * 
 * @author Tm
 *
 */
public class AGoods extends BasePojo<AGoods> {
	
	private int id;
	private String name;
	private double price;
	private double totalPrice;
	private int perLong;
	private int weekLong;
	private int peoNum;
	private byte souType;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	private String statusStr;
	private String souTypeStr ; 

	/* 关联对象的引用 */
	/*
	 * 方便枚举项在网页上显示出来 键为值(数字), 值为字符串描述 只提供get方法
	 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap() {
		// 根据状态值获取字符串描述
		AGoodsEnum[] adminsEnums = AGoodsEnum.values();
		for (int i = 0; i < adminsEnums.length; i++) {
			AGoodsEnum adminsEnum = adminsEnums[i];
			String key = adminsEnum.toString();
			enumsMap.put(key + "-" + adminsEnum.getStatus() + "", adminsEnum.getName());
		}
		return enumsMap;
	}

	public String getStatusStr() {
		AGoodsEnum[] values = AGoodsEnum.values();
		for (int i = 0; i < values.length; i++) {
			AGoodsEnum adminsEnumTemp = values[i];
			if (adminsEnumTemp.toString().startsWith("STATUS")) {
				if (adminsEnumTemp.getStatus() == this.status) {
					this.statusStr = adminsEnumTemp.getName();
				}
			}
		}

		return statusStr;
	}
	
	public String getSouTypeStr() {
		AGoodsEnum[] values = AGoodsEnum.values();
		for (int i = 0; i < values.length; i++) {
			AGoodsEnum adminsEnumTemp = values[i];
			if (adminsEnumTemp.toString().startsWith("SOUTYPE_")) {
				if (adminsEnumTemp.getStatus() == this.status) {
					this.souTypeStr = adminsEnumTemp.getName();
				}
			}
		}

		return souTypeStr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getPerLong() {
		return perLong;
	}

	public void setPerLong(int perLong) {
		this.perLong = perLong;
	}

	public int getWeekLong() {
		return weekLong;
	}

	public void setWeekLong(int weekLong) {
		this.weekLong = weekLong;
	}

	public int getPeoNum() {
		return peoNum;
	}

	public void setPeoNum(int peoNum) {
		this.peoNum = peoNum;
	}

	public byte getSouType() {
		return souType;
	}

	public void setSouType(byte souType) {
		this.souType = souType;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getPubTime() {
		return pubTime;
	}

	public void setPubTime(Date pubTime) {
		this.pubTime = pubTime;
	}
}
