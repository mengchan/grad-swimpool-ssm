package com.wangsh.clazz.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.common.pojo.BasePojo;

/**
 * 课程概要 Pojo
 * 
 * @author Tm
 *
 */
public class AClassDesc extends BasePojo<AClassDesc>
{
	private int id;
	private int goodsId;
	private Date currBeginDate;
	private Date currEndDate;
	private double price;
	private int peoNum;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	private String statusStr;

	/* 关联对象的引用 */
	private AGoods goods ;
	
	/*
	 * 方便枚举项在网页上显示出来 键为值(数字), 值为字符串描述 只提供get方法
	 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AClassDescEnum[] adminsEnums = AClassDescEnum.values();
		for (int i = 0; i < adminsEnums.length; i++)
		{
			AClassDescEnum adminsEnum = adminsEnums[i];
			String key = adminsEnum.toString();
			enumsMap.put(key + "-" + adminsEnum.getStatus() + "", adminsEnum.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		AClassDescEnum[] values = AClassDescEnum.values();
		for (int i = 0; i < values.length; i++)
		{
			AClassDescEnum adminsEnumTemp = values[i];
			if (adminsEnumTemp.toString().startsWith("STATUS"))
			{
				if (adminsEnumTemp.getStatus() == this.status)
				{
					this.statusStr = adminsEnumTemp.getName();
				}
			}
		}

		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getGoodsId()
	{
		return goodsId;
	}

	public void setGoodsId(int goodsId)
	{
		this.goodsId = goodsId;
	}

	public Date getCurrBeginDate()
	{
		return currBeginDate;
	}

	public void setCurrBeginDate(Date currBeginDate)
	{
		this.currBeginDate = currBeginDate;
	}

	public Date getCurrEndDate()
	{
		return currEndDate;
	}

	public void setCurrEndDate(Date currEndDate)
	{
		this.currEndDate = currEndDate;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public int getPeoNum()
	{
		return peoNum;
	}

	public void setPeoNum(int peoNum)
	{
		this.peoNum = peoNum;
	}

	public AGoods getGoods()
	{
		return goods;
	}

	public void setGoods(AGoods goods)
	{
		this.goods = goods;
	}

}
