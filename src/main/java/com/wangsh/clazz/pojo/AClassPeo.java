package com.wangsh.clazz.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.common.pojo.BasePojo;
import com.wangsh.users.pojo.AUsers;

/**
 * 课程学生 Pojo
 * 
 * @author Tm
 *
 */
public class AClassPeo extends BasePojo<AClassPeo>
{
	private int id;
	private int classId;
	private int usersId;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	private String statusStr;

	/* 关联对象的引用 */


	private AUsers users;

	private AClassDesc classDesc;
	/*
	 * 方便枚举项在网页上显示出来 键为值(数字), 值为字符串描述 只提供get方法
	 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AClassDescEnum[] adminsEnums = AClassDescEnum.values();
		for (int i = 0; i < adminsEnums.length; i++)
		{
			AClassDescEnum adminsEnum = adminsEnums[i];
			String key = adminsEnum.toString();
			enumsMap.put(key + "-" + adminsEnum.getStatus() + "", adminsEnum.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		AClassDescEnum[] values = AClassDescEnum.values();
		for (int i = 0; i < values.length; i++)
		{
			AClassDescEnum adminsEnumTemp = values[i];
			if (adminsEnumTemp.toString().startsWith("STATUS"))
			{
				if (adminsEnumTemp.getStatus() == this.status)
				{
					this.statusStr = adminsEnumTemp.getName();
				}
			}
		}

		return statusStr;
	}


	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classId)
	{
		this.classId = classId;
	}

	public int getUsersId()
	{
		return usersId;
	}

	public void setUsersId(int usersId)
	{
		this.usersId = usersId;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}

	public AClassDesc getClassDesc()
	{
		return classDesc;
	}

	public void setClassDesc(AClassDesc classDesc)
	{
		this.classDesc = classDesc;
	}
}
