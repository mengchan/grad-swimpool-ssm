package com.wangsh.clazz.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.common.pojo.BasePojo;
import com.wangsh.users.pojo.AUsers;

/**
 * 课程支付 Pojo
 * 
 * @author Tm
 *
 */
public class AClassPay extends BasePojo<AClassPay>
{
	private int id;
	private int goodsId;
	private int classId;
	private int usersId;
	private Date beginTime;
	private Date endTime;
	private int useClassNum;
	private int totalClassNum;
	private double price;
	private double totalPrice;
	private double discount;
	private double dealPrice;
	private byte payType;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date pubTime;

	private String statusStr;
	private String payTypeStr;

	/**
	 * 关联对象
	 */
	private AGoods goods;

	private AClassDesc classDesc;

	private AUsers users;
	/* 关联对象的引用 */
	/*
	 * 方便枚举项在网页上显示出来 键为值(数字), 值为字符串描述 只提供get方法
	 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AClassPayEnum[] adminsEnums = AClassPayEnum.values();
		for (int i = 0; i < adminsEnums.length; i++)
		{
			AClassPayEnum adminsEnum = adminsEnums[i];
			String key = adminsEnum.toString();
			enumsMap.put(key + "-" + adminsEnum.getStatus() + "", adminsEnum.getName());
		}
		return enumsMap;
	}

	public String getStatusStr()
	{
		AClassPayEnum[] values = AClassPayEnum.values();
		for (int i = 0; i < values.length; i++)
		{
			AClassPayEnum adminsEnumTemp = values[i];
			if (adminsEnumTemp.toString().startsWith("STATUS"))
			{
				if (adminsEnumTemp.getStatus() == this.status)
				{
					this.statusStr = adminsEnumTemp.getName();
				}
			}
		}

		return statusStr;
	}

	public String getPayTypeStr()
	{
		AClassPayEnum[] values = AClassPayEnum.values();
		for (int i = 0; i < values.length; i++)
		{
			AClassPayEnum adminsEnumTemp = values[i];
			if (adminsEnumTemp.toString().startsWith("PAYTYPE_"))
			{
				if (adminsEnumTemp.getStatus() == this.payType)
				{
					this.payTypeStr = adminsEnumTemp.getName();
				}
			}
		}

		return payTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getGoodsId()
	{
		return goodsId;
	}

	public void setGoodsId(int goodsId)
	{
		this.goodsId = goodsId;
	}

	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classId)
	{
		this.classId = classId;
	}

	public int getUsersId()
	{
		return usersId;
	}

	public void setUsersId(int usersId)
	{
		this.usersId = usersId;
	}

	public Date getBeginTime()
	{
		return beginTime;
	}

	public void setBeginTime(Date beginTime)
	{
		this.beginTime = beginTime;
	}

	public Date getEndTime()
	{
		return endTime;
	}

	public void setEndTime(Date endTime)
	{
		this.endTime = endTime;
	}

	public int getUseClassNum()
	{
		return useClassNum;
	}

	public void setUseClassNum(int useClassNum)
	{
		this.useClassNum = useClassNum;
	}

	public int getTotalClassNum()
	{
		return totalClassNum;
	}

	public void setTotalClassNum(int totalClassNum)
	{
		this.totalClassNum = totalClassNum;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	public Date getPubTime()
	{
		return pubTime;
	}

	public void setPubTime(Date pubTime)
	{
		this.pubTime = pubTime;
	}

	public void setEnumsMap(Map<String, String> enumsMap)
	{
		this.enumsMap = enumsMap;
	}

	public double getDiscount()
	{
		return discount;
	}

	public void setDiscount(double discount)
	{
		this.discount = discount;
	}

	public double getDealPrice()
	{
		return dealPrice;
	}

	public void setDealPrice(double dealPrice)
	{
		this.dealPrice = dealPrice;
	}

	public byte getPayType()
	{
		return payType;
	}

	public void setPayType(byte payType)
	{
		this.payType = payType;
	}

	public AClassDesc getClassDesc()
	{
		return classDesc;
	}

	public void setClassDesc(AClassDesc classDesc)
	{
		this.classDesc = classDesc;
	}

	public AGoods getGoods()
	{
		return goods;
	}

	public void setGoods(AGoods goods)
	{
		this.goods = goods;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}

}
