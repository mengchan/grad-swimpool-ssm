package com.wangsh.clazz.pojo;

/**
 * 管理员 枚举
 * @author Tm
 *
 */
public enum AGoodsEnum
{

	STATUS_DISABLED("disable", Byte.valueOf("0")),
	STATUS_ENABLE("enable", Byte.valueOf("1")), 
	
	/* 类型:0:课程,1:会员;2:年龄段 */
	SOUTYPE_CLASS("class", Byte.valueOf("0")),
	SOUTYPE_MEMBER("member", Byte.valueOf("1")),
	SOUTYPE_AGE("age", Byte.valueOf("2")),
	;

	private String name;
	private byte status;

	private AGoodsEnum(String name, byte status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
}
