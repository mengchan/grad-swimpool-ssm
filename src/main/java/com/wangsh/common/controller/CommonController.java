package com.wangsh.common.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wangsh.common.util.ConstatFinalUtil;
import com.wangsh.common.util.VerifyCodeUtils;

/**
 * 公共的方法
 * @author WangshSxt
 */
@Controller
@RequestMapping("/common/")
public class CommonController extends BaseController
{
	/**
	 * 生成验证码
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/randImg")
	public void randImg(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		response.setHeader("Pragma", "No-cache");  
       response.setHeader("Cache-Control", "no-cache");  
       response.setDateHeader("Expires", 0);  
       response.setContentType("image/jpeg");  
         
       //生成随机字串  
       String verifyCode = VerifyCodeUtils.generateVerifyCode(4);  
       //存入会话session  
       HttpSession session = request.getSession(true);  
       session.setAttribute("randSess", verifyCode);  
       //生成图片  
       int w = 200, h = 80;  
       VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), verifyCode); 
	}
	
	/**
	 * 生成验证码
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/info")
	public String info(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		ConstatFinalUtil.SYS_LOGGER.info("==info==");
		return "/info" ; 
	}
}
