package com.wangsh.common.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.wangsh.common.pojo.ApiResponseEnum;
import com.wangsh.common.util.ConstatFinalUtil;
import com.wangsh.common.util.DateFormatUtil;
import com.wangsh.common.util.EncryptUtil;
import com.wangsh.common.util.FileUtil;
import com.wangsh.common.util.PageInfoUtil;

/**
 * 所有Controller的父类
 * 
 * @author WangshSxt
 *
 */
public class BaseController {
	@Resource
	protected DateFormatUtil dateFormatUtil;
	@Resource
	protected EncryptUtil encryptUtil;
	@Resource
	protected FileUtil fileUtil;
	/**
	 * 页面上提示信息默认为null
	 */
	protected String info = ConstatFinalUtil.INFO_JSON.getString(ApiResponseEnum.STATUS_FAILED.getName());

	/**
	 * 公共的一些操作
	 * 
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	protected void commonOper(HttpServletRequest request) {
		/*
		 * 存储一些公共的变量 websiteUrl(网络路径),http://127.0.0.1:8080/usersCenter-web-head/
		 * websiteFileUrl(网路路径),http://127.0.0.1:8080/usersCenter-web-head/
		 */
		request.setAttribute("websiteUrl", ConstatFinalUtil.CONFIG_JSON.get("website.url"));
		request.setAttribute("websiteFileUrl", ConstatFinalUtil.CONFIG_JSON.get("website.fileUrl"));
	}

	/**
	 * 生成一个查询列表公共的搜索条件
	 * 
	 * @param request
	 * @return
	 */
	protected Map<String, Object> proccedSearch(HttpServletRequest request) {
		Map<String, Object> condMap = new HashMap<String, Object>();
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		/* request只是可以查询到但是自己并没有存储所以要将查询条件存储到request中 */
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		/* 关键字 */
		if (keyword == null) {
			keyword = "";
		}
		/* 状态 */
		if (status == null) {
			status = "";
		}
		/* 查询的起始时间和结束时间 */
		Date stDate = null;
		Date edDate = null;
		if (st != null && !"".equalsIgnoreCase(st) && ed != null && !"".equalsIgnoreCase(ed)) {
			stDate = this.dateFormatUtil.strDateTime(st);
			edDate = this.dateFormatUtil.strDateTime(ed);
		}
		condMap.put("keyword", keyword);
		condMap.put("status", status);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		return condMap;
	}

	/**
	 * 生成分页对象
	 * 
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	protected PageInfoUtil proccedPageInfo(HttpServletRequest request) {
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		/* 分页 */
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		try {
			/*
			 * 将字符串转换成整数,有风险, 字符串为a,转换不成不整数
			 */
			pageInfoUtil.setCurrentPage(Integer.valueOf(currentPage));
			pageInfoUtil.setPageSize(Integer.valueOf(pageSize));
			return pageInfoUtil;
		} catch (NumberFormatException e) {
		}
		return pageInfoUtil;
	}

	/**
	 * 从session中获取用户或者管理员信息
	 * 
	 * @return
	 */
	protected Object findObjfromSession(HttpServletRequest request, String type) {
		HttpSession session = request.getSession();
		if ("users".equalsIgnoreCase(type)) {
			return session.getAttribute("users");
		} else if ("admins".equalsIgnoreCase(type)) {
			return session.getAttribute("admins");
		}
		return null;
	}

	/**
	 * 返回json字符串
	 * 
	 * @param request
	 * @param info
	 * @return
	 * @throws IOException
	 */
	public JSONObject printOut(HttpServletRequest request, HttpServletResponse response, String returnStr) {
		try {
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.print(returnStr);
			out.flush();
			out.close();
		} catch (IOException e) {
			ConstatFinalUtil.SYS_LOGGER.error("为客户端返回信息出错了;", e);
		}
		return null;
	}

	/**
	 * 获取ip
	 * 
	 * @param request
	 * @return
	 */
	public String getIp(HttpServletRequest request) {
		String ipAddress = request.getHeader("X-Real-IP");
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();
		}
		if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
			// 根据网卡取本机配置的IP
			InetAddress inet = null;
			try {
				inet = InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			ipAddress = inet.getHostAddress();
		}
		return ipAddress;
	}

	/**
	 * 生成验证码
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/info")
	public String info(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ConstatFinalUtil.SYS_LOGGER.info("==info==");
		return "/info";
	}
}
