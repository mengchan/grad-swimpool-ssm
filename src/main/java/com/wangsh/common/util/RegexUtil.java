package com.wangsh.common.util;

import java.util.Random;

import org.springframework.stereotype.Component;

/**
 * 和正则表达式相关的工具类
 * @author Wangsh
 */
@Component("regexUtil")
public class RegexUtil
{
	/**
	 * 生成随机字符串(a-zA-Z0-9)
	 * @param len 长度
	 * @return
	 */
	public String randStr(int len)
	{
		/* 4~ 存储结果 */
		StringBuffer sb = new StringBuffer() ; 
		/* 3~随机生成0-100的数 */
		Random random = new Random() ; 
		for (int i = 0; i < len; i++)
		{
			/* 1~让charAt中的数字,随机:范围是? */
			int randInt = random.nextInt(ConstatFinalUtil.ALLSTR.length());
			/* 2~指定位置上的char字符 */
			char ch = ConstatFinalUtil.ALLSTR.charAt(randInt);
			//System.out.println(ch + "---" + randInt);
			sb.append(ch);
		}
		return sb.toString() ; 
	}
	
	/**
	 * 比对验证码是否正确
	 * @return
	 */
	public boolean verifyCode(String sysCode,String code) 
	{
		return (sysCode != null && sysCode.equalsIgnoreCase(code)) 
		||                             //0715
		ConstatFinalUtil.CONFIG_JSON.getString("code.str").equalsIgnoreCase(code) ; 
	}
}
