package com.wangsh.common.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

/**
 * 加密的工具类
 * 
 * @author Wangsh
 */
@Component("encryptUtil")
public class EncryptUtil {
	/**
	 * 加密字符串
	 * 
	 * 加密算法$随机数$密文(加密算法加密(明文+随机数))
	 * 
	 * sha256$abcd$sha256(11111abcd)
	 * 
	 * @param souStr
	 * @return
	 */
	public String encodeStr(String souStr) {
		/* 分隔符 */
		String split = "$";
		/* 加密算法 */
		String encType = "sha256";
		/*
		 * 随机数 生成指定位数的随机数(字母+数字)
		 */
		RegexUtil regexUtil = new RegexUtil();
		String randStr = regexUtil.randStr(32);

		String jiamaHead = encType + split + randStr + split;

		/* 加密字符串 */
		String jiamiStr = DigestUtils.sha256Hex(souStr + randStr);
		return jiamaHead + jiamiStr;
	}

	/**
	 * 对比加密字符串
	 * 
	 * 拿着明文再加密一次,比对密文
	 * 
	 * 加密算法$随机数$密文(加密算法加密(明文+随机数))
	 * 
	 * @param souStr
	 * @return
	 */
	public boolean checkStr(String souStr, String encodeStr) {
		/* 分隔符 */
		String split = "$";
		/* 加密算法 */
		String encType = "sha256";
		/*
		 * 取随机数 按照$拆分 0:加密算法 1:随机数 2:加密后的部分字符串(用不到)
		 */
		String[] encodeStrs = encodeStr.split("\\" + split);
		/* 拆分后的字符串我只使用了第2个 */
		if (encodeStrs.length == 3) {
			/* 传入的密码必须含$ */
			/* 拼装结果的部分字符串 */
			String jiamaHead = encType + split + encodeStrs[1] + split;
			/* 加密字符串 */
			String jiamiStr = DigestUtils.sha256Hex(souStr + encodeStrs[1]);
			/*
			 * 拼装最终的字符串 加密算法$随机数$密文(加密算法加密(明文+随机数))
			 */
			String finalStr = jiamaHead + jiamiStr;

			/* 验证明文是否一致 */
			return finalStr.equalsIgnoreCase(encodeStr);
		}
		return false;
	}

	public static void main(String[] args) {
		EncryptUtil encryptUtil = new EncryptUtil();
		
		String soupass = "111111" ; 
		String res = encryptUtil.encodeStr(soupass);
		System.out.println(res);
		
		boolean flag = encryptUtil.checkStr(soupass, res);
		System.out.println("====>" + flag);
	}
}
