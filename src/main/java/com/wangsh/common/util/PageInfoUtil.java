package com.wangsh.common.util;

import java.io.Serializable;

import com.wangsh.common.pojo.BasePojo;

/**
 * 分页的工具类
 * 
 * @author Wangsh
 */
public class PageInfoUtil extends BasePojo<PageInfoUtil> implements Serializable {
	/* 总条数 */
	private int totalRecord;
	/* 每页多少条 */
	private int pageSize = 20;

	/* 总页数 */
	private int totalPage;
	/* 当前页 */
	private int currentPage;
	/* 上一页 */
	private int prePage;
	/* 下一页 */
	private int nextPage;

	private int currRecord;

	public int getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPage() {
		if (this.currentPage < 1) {
			this.currentPage = 1;
		}
		if (this.getTotalPage() > 0 && this.currentPage > this.getTotalPage()) {
			this.currentPage = this.getTotalPage();
		}
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getTotalPage() {
		if (this.totalRecord % this.pageSize == 0) {
			this.totalPage = this.totalRecord / this.pageSize;
		} else {
			this.totalPage = this.totalRecord / this.pageSize + 1;
		}
		return totalPage;
	}

	public int getPrePage() {
		this.prePage = this.getCurrentPage() - 1;
		if (this.prePage < 1) {
			this.prePage = 1;
		}
		return prePage;
	}

	public int getNextPage() {
		this.nextPage = this.getCurrentPage() + 1;
		if (this.getTotalPage() > 0 && this.nextPage > this.getTotalPage()) {
			this.nextPage = this.getTotalPage();
		}
		return nextPage;
	}

	public int getCurrRecord() {
		this.currRecord = (this.getCurrentPage() - 1) * this.pageSize;
		return currRecord;
	}

	public static void main(String[] args) {
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setTotalRecord(50);
		pageInfoUtil.setPageSize(10);
		pageInfoUtil.setCurrentPage(1000);
		System.out.println(pageInfoUtil);
	}
}
