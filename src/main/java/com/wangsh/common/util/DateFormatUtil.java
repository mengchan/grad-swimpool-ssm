package com.wangsh.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * 日期时间的工具类
 * 
 * @author Wangsh
 */
@Component("dateFormatUtil")
public class DateFormatUtil {
	/**
	 * 将日期变成字符串
	 * 
	 * @param date
	 * @return
	 */
	public String dateStr(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(ConstatFinalUtil.DATE_FORMAT);
		return sdf.format(date);
	}

	/**
	 * 将日期时间变成字符串
	 * 
	 * @param date
	 * @return
	 */
	public String dateTimeStr(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(ConstatFinalUtil.DATETIME_FORMAT);
		return sdf.format(date);
	}

	/**
	 * 将日期变成字符串
	 * 
	 * @param date
	 * @param pattern 日期时间的格式
	 * @return
	 */
	public String dateStr(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}

	/**
	 * 将字符串变成日期
	 * 
	 * @param date
	 * @return
	 */
	public Date strDate(String now) {
		SimpleDateFormat sdf = new SimpleDateFormat(ConstatFinalUtil.DATE_FORMAT);
		try {
			return sdf.parse(now);
		} catch (ParseException e) {
		}
		return null;
	}

	/**
	 * 将字符串变成日期时间
	 * 
	 * @param date
	 * @return
	 */
	public Date strDateTime(String now) {
		SimpleDateFormat sdf = new SimpleDateFormat(ConstatFinalUtil.DATETIME_FORMAT);
		try {
			return sdf.parse(now);
		} catch (ParseException e) {
			return new Date();
		}
	}

	/**
	 * 将日期变成字符串
	 * 
	 * @param date
	 * @param pattern 日期时间的格式
	 * @return
	 */
	public Date strDate(String now, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			return sdf.parse(now);
		} catch (ParseException e) {
			return new Date() ; 
		}
	}

	/**
	 * 获取传入的时间与当前时间的时间差(分钟)
	 * 
	 * @param date
	 * @return
	 */
	public long getTimeDifferenceMinute(Date date) {
		long currentTimeMillis = System.currentTimeMillis();
		long time = date.getTime();
		long timeDifference = (currentTimeMillis - time) / 60000;
		return timeDifference;
	}

	/**
	 * 获取传入的时间与当前时间的时间差(秒)
	 * 
	 * @param date
	 * @return
	 */
	public long getTimeDifferenceSecond(Date date) {
		long currentTimeMillis = System.currentTimeMillis();
		long time = date.getTime();
		long timeDifference = (time - currentTimeMillis) / 1000;
		return timeDifference;
	}
}
