package com.wangsh.common.util;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 全部是一些静态的常量
 * 
 * @author Wangsh
 */
public class ConstatFinalUtil {
	/* 定义日志对象 */
	public static final Logger SYS_LOGGER = LogManager.getLogger();
	/* 定义日志对象(Timer) */
	public static final Logger TIMER_LOGGER = LogManager.getLogger("TimerLog");
	/* 所有的字符串 */
	public static final String ALLSTR = "abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
	/* 日期的默认格式 */
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	/* 日期时间的默认格式 */
	public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	/* 拆分字符串 */
	public static final String SPLIT_STR = "|-->";
	/* 拆分字符串 */
	public static final String SPLIT_STR_ZHUAN = "\\|-->";
	/* 秒 */
	public static final int SECOND = 1000;
	/* 编码 */
	public static final String CHARSET = "UTF-8";
	/* 请求次数 */
	public static final int REQ_COUNT = 5;
	/* 请求服务器超时 */
	public static final int REQ_CONNECT_TIMEOUT = 5 * SECOND;
	/* 读取超时 */
	public static final int READ_TIMEOUT = 5 * SECOND;

	/* 配置文件中的json对象 */
	public static JSONObject SYS_JSON = new JSONObject();
	/* 配置文件中的json对象,Config模块 */
	public static JSONObject CONFIG_JSON = new JSONObject();
	/* 配置文件中的json对象,info模块 */
	public static JSONObject INFO_JSON = new JSONObject();

	/* 存储Token,管理员对应的信息 */
	public static Map<String, JSONObject> ADMINS_MAP = new HashMap<String, JSONObject>();
	/* 存放所有线程的容器 */
	public static Map<String, Map<String, Object>> THREAD_MAP = new Hashtable<String, Map<String, Object>>();
	/* 常见的请求头信息 */
	public static JSONArray HTTP_JSONARR = new JSONArray();

	/*
	 * 放一些初始化的操作 只执行一次
	 */
	static {
		/* FastJSON中默认的格式 */
		JSON.DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
		FileUtil fileUtil = new FileUtil();
		/*
		 * 从Classpath中获取 classpath在target下面,但是配置的时候让classpath在target下面不显示 workspace里面有,
		 * 编译后的class文件的相对路径是忽略包的 把ConstatFinalUtil所在class文件的包忽略掉,就和config.json在同一目录下了
		 */
		String result = fileUtil.readFile(ConstatFinalUtil.class.getClassLoader().getResourceAsStream("config.json"));
		try {
			SYS_JSON = (JSONObject) JSON.parse(result);
			CONFIG_JSON = SYS_JSON.getJSONObject("config");
			INFO_JSON = SYS_JSON.getJSONObject("info");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
