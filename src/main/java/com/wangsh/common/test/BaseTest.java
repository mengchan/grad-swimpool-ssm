package com.wangsh.common.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.wangsh.common.util.ConstatFinalUtil;

/**
 * 测试类的父类
 * 
 * @author Wangsh
 *
 */
public class BaseTest {
	/* Spring的应用上下文 */
	protected ApplicationContext ac;

	/**
	 * 初始化
	 */
	@Before
	public void init() {
		/**
		 * ClassPathXmlApplicationContext:默认只会从所在的项目中找配置文件
		 * classpath*:表示从所有的Classpath中寻找配置文件,包含JAR包
		 */
		/*
		 * ac = new
		 * ClassPathXmlApplicationContext("classpath*:spring/applicationContext-*.xml");
		 */
		ac = new ClassPathXmlApplicationContext("classpath*:spring/applicationContext-*.xml");
		ConstatFinalUtil.SYS_LOGGER.info("--init--ac:{}", ac);
	}

	/**
	 * 测试
	 */
	@Test
	public void test() {
		ConstatFinalUtil.SYS_LOGGER.info("--test--");
	}

	/**
	 * 关闭
	 */
	@After
	public void close() {
		ConstatFinalUtil.SYS_LOGGER.info("--close--");
		if (this.ac instanceof ClassPathXmlApplicationContext) {
			ClassPathXmlApplicationContext cpxac = (ClassPathXmlApplicationContext) this.ac;
			cpxac.close();
		}
	}
}
