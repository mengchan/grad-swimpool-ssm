package com.wangsh.common.pojo;

/**
 * 枚举
 * 
 * @author wangsh
 */
public enum ApiResponseEnum {
	
	STATUS_SUCCESS(Byte.valueOf("0"), "success"),
	STATUS_SERVER_ERROR(Byte.valueOf("-1"), "System error"), 
	STATUS_FAILED(Byte.valueOf("1"), "fail"),
	
	/* 提示信息 */
	INFO_EMAIL_EXISTS(Byte.valueOf("2"), "\r\n"
			+ "Email exists, please change another one"),
	INFO_ACCOUNT_DISABLED(Byte.valueOf("3"), "Account disable"),
	INFO_PASS_INCORRECT(Byte.valueOf("4"), "The password is incorrect"),
	INFO_EMAIL_NOT_EXISTS(Byte.valueOf("5"), "用户邮箱不存在"),
	INFO_VERIFY_CODE_INCORR(Byte.valueOf("6"), "验证码不正确"),
	INFO_LOGIN_ILLEGAL(Byte.valueOf("7"), "非法访问,请先登陆"),
	INFO_UPLOAD_DATA_EMPTY(Byte.valueOf("9"), "请输入上行参数"),
	INFO_JSON_FORMAT_ERROR(Byte.valueOf("10"),"上传的json数据格式不正确,信息:${info}"),
	INFO_JSON_VERSION_ERROR(Byte.valueOf("11"),"版本号不正确"),
	INFO_JSON_SERVER_ERROR(Byte.valueOf("13"),"版本号不正确"),
	INFO_JSON_ATTACK(Byte.valueOf("14"),"恶意攻击"),
	INFO_EMAIL_NO_EMPTY(Byte.valueOf("16"),"邮箱不能为空"),
	INFO_EMAIL_PASSED(Byte.valueOf("18"), "邮箱已经认证"),
	INFO_EMAIL_VERIY_HREF_TIMEOUT(Byte.valueOf("19"), "邮箱链接已经超过一天"),
	INFO_EMAIL_VERIY_HREF_NOTSEC(Byte.valueOf("20"), "邮箱链接已经被篡改"),
	INFO_SOUPASS_INCORRECT(Byte.valueOf("21"), "原密码不正确"),
	INFO_REGISTER_SUCCESS(Byte.valueOf("23"), "注册成功,稍后到邮箱中验证邮箱"),
	INFO_EMAIL_VERIFY_SUCCESS(Byte.valueOf("24"), "邮箱验证成功"),
	INFO_EMAIL_SENDED(Byte.valueOf("25"), "邮箱验证成功"),
	INFO_LOGOUT(Byte.valueOf("26"), "退出成功"),
	INFO_METHOD_ERROR(Byte.valueOf("27"), "方法名不存在"),
	INFO_AUTH_NOT(Byte.valueOf("28"), "您木有权限访问,请联系管理员"),
	INFO_EMAIL_VERIFY_FAILED(Byte.valueOf("29"),"无此邮箱,或者该邮箱qq、电话不匹配"),
	INFO_VERIFY_FAILED(Byte.valueOf("30"),"验证码已过期"),
	INFO_LOGIN_REPETITION(Byte.valueOf("31"),"该账号已被异地登录"),
	INFO_LOGIN_NOT_OPERATING(Byte.valueOf("32"),"长时间未操作,已自动退出"),
	INFO_ERROR_FORMAT(Byte.valueOf("33"), "格式不正确"),
	INFO_RECORD_EXISTS(Byte.valueOf("34"), "记录已经存在"),
	INFO_RECORD_NOTEXISTS(Byte.valueOf("35"), "记录不存在"),
	INFO_PUBKEY_ERROR(Byte.valueOf("36"), "公钥错误"),
	INFO_SIGN_ERROR(Byte.valueOf("37"), "签名错误"),
	
	/* 所有和demoDynasty相关的变量 */
	NAME_LIST(Byte.valueOf("0"), "list"),
	NAME_ONE(Byte.valueOf("0"), "one") , 
	NAME_PAGEINFOUTIL(Byte.valueOf("0"), "pageInfoUtil"),
	
	USERTYPE_USER(Byte.valueOf("0"), "用户"),
	USERTYPE_STAFF(Byte.valueOf("1"), "员工"),
	USERTYPE_ADMINS(Byte.valueOf("2"), "管理员"),
	;
	
	private byte status;
	private String name;

	private ApiResponseEnum(byte status, String name) {
		this.status = status;
		this.name = name;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
