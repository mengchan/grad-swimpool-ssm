package com.wangsh.common.pojo;

/**
 * 枚举
 * @author wangsh
 */
public enum JSONEnum
{
	EFFECT(Byte.valueOf("0"), "effect"), 
	ID(Byte.valueOf("1"), "id");
	
	private byte status;
	private String name;

	private JSONEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
