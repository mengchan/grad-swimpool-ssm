package com.wangsh.common.pojo;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * 所有POJO的超类
 * 
 * @author wangsh
 */
public class BasePojo<T> implements Serializable {
	/**
	 * 将对象转换为JSON
	 * 
	 * @return
	 */
	public JSONObject toJSON() {
//		SerializeConfig config = new SerializeConfig() ;
//		config.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
//		return (JSONObject) JSON.toJSON(this,config);
		String jsonStr = JSON.toJSONString(this, SerializerFeature.WriteDateUseDateFormat);
		return (JSONObject) JSON.parse(jsonStr);
	}

	/**
	 * 从json对象中解析对象
	 * 
	 * @param souStr
	 * @return
	 */
	protected T parseJSON(JSONObject souJSON) {
		return null;
	}
}
