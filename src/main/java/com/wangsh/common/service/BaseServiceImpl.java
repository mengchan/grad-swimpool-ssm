package com.wangsh.common.service;

import javax.annotation.Resource;

import com.wangsh.common.util.DateFormatUtil;
import com.wangsh.common.util.EncryptUtil;
import com.wangsh.common.util.FileUtil;
import com.wangsh.common.util.RegexUtil;

/**
 * 所有Service的子类
 * @author Wangsh
 */
public class BaseServiceImpl
{
	@Resource
	protected DateFormatUtil dateFormatUtil ;
	@Resource
	protected FileUtil fileUtil ; 
	@Resource
	protected RegexUtil regexUtil ;
	@Resource
	protected EncryptUtil encryptUtil ;
}
