package com.wangsh.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.common.pojo.BasePojo;

/**
 * 管理员 Pojo
 * 
 * @author Tm
 *
 */
public class AAdmins extends BasePojo<AAdmins> {
	// 主键id
	private int id;
	// 邮箱
	private String email;
	// 密码
	private String password;
	// 名称
	private String name;
	// 联系方式
	private String phone;
	// 状态:0-禁用 1-启用
	private byte status;
	// 创建时间
	private Date createTime;
	// 更新时间
	private Date updateTime;
	// 上次登陆时间
	private Date lastLoginTime;

	private String statusStr;

	/* 关联对象的引用 */
	/*
	 * 方便枚举项在网页上显示出来 键为值(数字), 值为字符串描述 只提供get方法
	 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap() {
		// 根据状态值获取字符串描述
		AAdminsEnum[] adminsEnums = AAdminsEnum.values();
		for (int i = 0; i < adminsEnums.length; i++) {
			AAdminsEnum adminsEnum = adminsEnums[i];
			String key = adminsEnum.toString();
			enumsMap.put(key + "-" + adminsEnum.getStatus() + "", adminsEnum.getName());
		}
		return enumsMap;
	}

	public String getStatusStr() {
		AAdminsEnum[] values = AAdminsEnum.values();
		for (int i = 0; i < values.length; i++) {
			AAdminsEnum adminsEnumTemp = values[i];
			if (adminsEnumTemp.toString().startsWith("STATUS")) {
				if (adminsEnumTemp.getStatus() == this.status) {
					this.statusStr = adminsEnumTemp.getName();
				}
			}
		}

		return statusStr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
}
