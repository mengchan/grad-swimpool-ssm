package com.wangsh.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wangsh.common.pojo.BasePojo;

/**
 * 用户 Pojo
 * 
 * @author Tm
 *
 */
public class AUsers extends BasePojo<AUsers> {
	private int id;
	private String email;
	private String password;
	private String name;
	private String nickName;
	private String phone;
	private int loginCount;
	private Date birthday;
	private String school;
	private byte memFlag ; 
	private byte sex;
	private byte status;
	private Date createTime;
	private Date updateTime;
	private Date lastLoginTime;

	private String statusStr;
	private String sexStr;
	private String memFlagStr ; 
	/* 年龄 */
	private int age ; 
	/* 存储所有状态的容器 */
	private Map<String, String> enumsMap = new TreeMap<String, String>();

	/**
	 * 关联对象
	 */
	public Map<String, String> getEnumsMap() {
		// 根据状态值获取字符串描述
		AUsersEnum[] usersEnums = AUsersEnum.values();
		for (int i = 0; i < usersEnums.length; i++) {
			AUsersEnum usersEnum = usersEnums[i];
			String key = usersEnum.toString();
			enumsMap.put(key + "-" + usersEnum.getStatus() + "", usersEnum.getName());
		}
		return enumsMap;
	}

	public String getStatusStr() {
		AUsersEnum[] values = AUsersEnum.values();
		for (int i = 0; i < values.length; i++) {
			AUsersEnum usersEnumTemp = values[i];
			/*
			 * toString:变量名 name:枚举的键, getstatus:枚举的值
			 */
			if (usersEnumTemp.toString().startsWith("STATUS")) {
				if (usersEnumTemp.getStatus() == this.status) {
					this.statusStr = usersEnumTemp.getName();
				}
			}
		}
		return statusStr;
	}

	public String getSexStr() {
		AUsersEnum[] values = AUsersEnum.values();
		for (int i = 0; i < values.length; i++) {
			AUsersEnum usersEnumTemp = values[i];
			/*
			 * toString:变量名 name:枚举的键, getstatus:枚举的值
			 */
			if (usersEnumTemp.toString().startsWith("SEX")) {
				if (usersEnumTemp.getStatus() == this.sex) {
					this.sexStr = usersEnumTemp.getName();
				}
			}
		}
		return sexStr;
	}
	
	public String getMemFlagStr() {
		AUsersEnum[] values = AUsersEnum.values();
		for (int i = 0; i < values.length; i++) {
			AUsersEnum usersEnumTemp = values[i];
			/*
			 * toString:变量名 name:枚举的键, getstatus:枚举的值
			 */
			if (usersEnumTemp.toString().startsWith("MEMFLAG_")) {
				if (usersEnumTemp.getStatus() == this.memFlag) {
					this.memFlagStr = usersEnumTemp.getName();
				}
			}
		}
		return memFlagStr;
	}
	
	/**
	 * 年龄
	 * @return
	 */
	public int getAge()
	{
		long age = 0 ;
		/* 差价 */
		long cha = System.currentTimeMillis() - this.getBirthday().getTime() ;
		/* 计算年龄 */
		age = cha / 1000 / 60 / 60 / 24 / 365; 
		return Long.valueOf(age).intValue() ; 
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public void setEnumsMap(Map<String, String> enumsMap) {
		this.enumsMap = enumsMap;
	}

	public byte getMemFlag()
	{
		return memFlag;
	}

	public void setMemFlag(byte memFlag)
	{
		this.memFlag = memFlag;
	}
}
