package com.wangsh.users.pojo;

/**
 * 管理员 枚举
 * @author Tm
 *
 */
public enum AAdminsEnum {

	STATUS_DISABLED("disable", Byte.valueOf("0")),
	STATUS_ENABLE("enable", Byte.valueOf("1")), 
	
	;

	private String name;
	private byte status;

	private AAdminsEnum(String name, byte status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

}
