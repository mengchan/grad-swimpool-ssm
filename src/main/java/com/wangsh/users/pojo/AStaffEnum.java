package com.wangsh.users.pojo;


/**
 * 用户 枚举
 * @author Tm
 *
 */
public enum AStaffEnum {
	/*
	 * 定义枚举项
	 */
	SEX_UNKNOW("unknow", Byte.valueOf("0")), 
	SEX_MAN("boy", Byte.valueOf("1")), 
	SEX_WOMEN("girl", Byte.valueOf("2")),

	STATUS_DISABLED("disable", Byte.valueOf("0")),
	STATUS_ENABLE("enable", Byte.valueOf("1")), 
	
	;

	private String name;
	private byte status;

	private AStaffEnum(String name, byte status)
	{
		this.name = name;
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

}
