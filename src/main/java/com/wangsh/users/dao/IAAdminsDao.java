package com.wangsh.users.dao;

import com.wangsh.common.dao.IBaseDao;
import com.wangsh.users.pojo.AAdmins;

/**
 * 管理员 dao
 * 
 * @author Tm
 */
public interface IAAdminsDao extends IBaseDao<AAdmins> {
	
}
