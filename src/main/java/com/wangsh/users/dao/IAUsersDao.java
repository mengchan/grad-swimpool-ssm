package com.wangsh.users.dao;

import com.wangsh.common.dao.IBaseDao;
import com.wangsh.users.pojo.AUsers;

/**
 * 用户 dao
 * @author Tm
 *
 */
public interface IAUsersDao extends IBaseDao<AUsers> {

}
