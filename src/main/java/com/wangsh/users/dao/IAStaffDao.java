package com.wangsh.users.dao;

import com.wangsh.common.dao.IBaseDao;
import com.wangsh.users.pojo.AStaff;

/**
 * 员工 dao
 * @author Tm
 *
 */
public interface IAStaffDao extends IBaseDao<AStaff> {

}
