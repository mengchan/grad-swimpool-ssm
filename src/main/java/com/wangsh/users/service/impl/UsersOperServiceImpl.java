package com.wangsh.users.service.impl;

import org.springframework.stereotype.Service;

import com.wangsh.common.service.BaseServiceImpl;
import com.wangsh.users.service.IUsersOperService;

/**
 * 用户 Oper Service Impl
 * @author Tm
 *
 */
@Service("usersOperService")
public class UsersOperServiceImpl<T> extends BaseServiceImpl implements IUsersOperService {
}
