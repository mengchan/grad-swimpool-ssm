package com.wangsh.users.service;

import java.util.Map;

import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.util.PageInfoUtil;
import com.wangsh.users.pojo.AAdmins;
import com.wangsh.users.pojo.AStaff;
import com.wangsh.users.pojo.AUsers;

/**
 * 用户 Db Service
 * @author Tm
 *
 */
public interface IUsersDbService {
	/* == 管理员操作开始 == */
	/**
	 * 保存一条管理员
	 * 
	 * @param admins
	 * @return
	 */
	ApiResponse<Object> saveOneAdminsService(AAdmins admins);

	/**
	 * 更新一条管理员
	 * 
	 * @param admins
	 * @return
	 */
	ApiResponse<Object> updateOneAdminsService(AAdmins admins);

	/**
	 * 删除一条管理员
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> deleteOneAdminsService(Map<String, Object> condMap);

	/**
	 * 查询一条管理员
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AAdmins> findOneAdminsService(Map<String, Object> condMap);

	/**
	 * 查询多条管理员
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AAdmins> findCondListAdminsService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);

	/* == 管理员操作结束 == */

	/* == 用户操作开始 == */

	ApiResponse<Object> saveOneUsersService(AUsers users);

	/**
	 * 更新一条用户
	 * 
	 * @param Users
	 * @return
	 */
	ApiResponse<Object> updateOneUsersService(AUsers users);

	/**
	 * 更新一条用户
	 * 
	 * @param Users
	 * @param paramsMap 其它辅助信息
	 * @return
	 */
	ApiResponse<Object> updateOneUsersService(AUsers users, Map<String, Object> paramsMap);

	/**
	 * 删除一条用户
	 * 
	 * @param users
	 * @return
	 */
	ApiResponse<Object> deleteOneUsersService(Map<String, Object> condMap);

	/**
	 * 查询一条用户
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsers> findOneUsersService(Map<String, Object> condMap);

	/**
	 * 查询多条用户
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AUsers> findCondListUsersService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/* == 用户操作结束 == */
	
	/* == 员工操作开始 == */
	/**
	 * 保存一条员工
	 * 
	 * @param staff
	 * @return
	 */
	ApiResponse<Object> saveOneStaffService(AStaff staff);

	/**
	 * 更新一条员工
	 * 
	 * @param staff
	 * @return
	 */
	ApiResponse<Object> updateOneStaffService(AStaff staff);

	/**
	 * 删除一条员工
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<Object> deleteOneStaffService(Map<String, Object> condMap);

	/**
	 * 查询一条员工
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AStaff> findOneStaffService(Map<String, Object> condMap);

	/**
	 * 查询多条员工
	 * 
	 * @param condMap
	 * @return
	 */
	ApiResponse<AStaff> findCondListStaffService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);

	/* == 员工操作结束 == */
}
