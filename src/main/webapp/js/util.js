var rootPath = "/swimpool-ssm/" ; 

/**
打开一个网址
 */
function openUrl(url,newBlank)
{
	url = rootPath + url ; 
	
	if(newBlank == "blank")
	{
		/* 打开一个新的网页 */
		window.open(url);
	}else
	{
		window.location.href = url ; 
	}
}

/**
	存储数据到本地
 */
function setData(key,data)
{
	localStorage.setItem(key,data);
}

/**
获取数据
 */
function getData(key)
{
	return localStorage.getItem(key);
}

/**
删除数据
 */
function removeData(key)
{
	return localStorage.removeItem(key);
}

/**
	退出
 */
function logout(usersType)
{
	if(!window.confirm('confirm logout?'))
	{
		return ; 
	}
	removeData(usersType);
	window.location.href = rootPath + '/back/login.html' ;
	return false ; 
}

/**
验证登录
 */
function verify(usersType)
{
	var obj = getData(usersType);
	if(obj == null || obj == '' || obj == undefined)
	{
		/* 退出登录 */
		window.location.href = rootPath + '/back/login.html' ;
	}
	return obj ; 
}

/**
获取url参数
 */
function urlParam(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

/*======select Data====== */
function goodsSelect(selectId)
{
	var postParam = $("#pageForm").serialize();
	/* 加载数据 */
	$.post(
		rootPath + "/class/goodsList.mvc",
		postParam + "&pageFlag=false",
		function(data)
		{
			var dataList = data.data.list ; 
			var sb = "" ;
			for(i = 0 ;i < dataList.length ; i ++)
			{
				var dataTemp = dataList[i] ; 
				/* oper */
				sb += "<option value='"+ dataTemp.id +"'>"+ dataTemp.name +"</a>" ; 
			}
			/* 数据show */
			$("#" + selectId).html(sb);
		},
		"json"
	);
	return false ; 
}