package com.wangsh.users.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.wangsh.common.pojo.ApiResponse;
import com.wangsh.common.test.BaseTest;
import com.wangsh.common.util.ConstatFinalUtil;
import com.wangsh.common.util.PageInfoUtil;
import com.wangsh.users.pojo.AAdmins;
import com.wangsh.users.service.IUsersDbService;

/**
 * UsersService的测试类
 * 
 * @author Tm
 *
 */
public class UsersDbServiceTest extends BaseTest {
	private IUsersDbService usersDbService;

	/**
	 * 初始化
	 */
	@Before
	public void init() {
		/*
		 * 为啥要调用父类的方法 因为了重写
		 */
		super.init();
		this.usersDbService = (IUsersDbService) this.ac.getBean("usersDbService");
		// this.logger.info("adminsService:{}",adminsService);
	}

	/**
	 * 保存一条管理员
	 */
	@Test
	public void saveOneAdminsService() {
		AAdmins admins = new AAdmins();
		admins.setEmail("11@11.com");
		admins.setPassword("11");
		admins.setName("张");
		admins.setPhone("5456555");
		admins.setStatus(Byte.valueOf("0"));
		admins.setCreateTime(new Date());
		admins.setUpdateTime(new Date());
		admins.setLastLoginTime(new Date());
		ApiResponse<Object> apiResponse = this.usersDbService.saveOneAdminsService(admins);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}", apiResponse.getCode(), apiResponse.getInfo(),
				apiResponse.toJSON());
	}

	/**
	 * 更新一条管理员
	 */
	@Test
	public void updateOneAdminsService() {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "2");
		ApiResponse<AAdmins> apiResonseRes = this.usersDbService.findOneAdminsService(condMap);
		AAdmins users = apiResonseRes.getDataOneJava();
		/* 修改值 */
		users.setEmail(users.getEmail() + "_aaa");
		users.setUpdateTime(new Date());

		ApiResponse<Object> apiResponse = this.usersDbService.updateOneAdminsService(users);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}", apiResponse.getCode(), apiResponse.getInfo(),
				apiResponse.toJSON());
	}

	/**
	 * 删除一条用户
	 */
	@Test
	public void deleteOneAdminsService() {
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 按照Id查询 */
		condMap.put("id", "5");
		ApiResponse<Object> apiResponse = this.usersDbService.deleteOneAdminsService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}", apiResponse.getCode(), apiResponse.getInfo(),
				apiResponse.toJSON());
	}

	/**
	 * 查询一条管理员
	 */
	@Test
	public void findOneAdminsService() {
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		ApiResponse<AAdmins> apiResponse = this.usersDbService.findOneAdminsService(condMap);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}", apiResponse.getCode(), apiResponse.getInfo(),
				apiResponse.toJSON());
	}

	/**
	 * 查询多条管理员
	 */
	@Test
	public void findCondListAdminsService() {
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 分页 */
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setCurrentPage(1000000);
		/* 搜索的条件 */
		// condMap.put("keyword", "2");
		// condMap.put("status", "0");
		// condMap.put("stDate", new Date());
		// condMap.put("edDate", new Date());
		/*
		 * List<AAdmins> adminsList = this.usersService.findCondListService(null ,
		 * condMap);
		 */
		ApiResponse<AAdmins> apiResponse = this.usersDbService.findCondListAdminsService(pageInfoUtil, condMap);
		ConstatFinalUtil.SYS_LOGGER.info("code:{},信息:{},记录:{}", apiResponse.getCode(), apiResponse.getInfo(),
				apiResponse.toJSON());
		ConstatFinalUtil.SYS_LOGGER.info("pageInfoUtil:{}", pageInfoUtil.toJSON());

		List<AAdmins> usersList = apiResponse.getDataListJava();
		for (Iterator iterator = usersList.iterator(); iterator.hasNext();) {
			AAdmins admins = (AAdmins) iterator.next();
			ConstatFinalUtil.SYS_LOGGER.info("admins:id:{},email:{}", admins.getId(), admins.getEmail());
		}

		ConstatFinalUtil.SYS_LOGGER.info("分页信息:{}", pageInfoUtil);
	}
}
